package is.katas.lambdas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Carlos",20));
        persons.add(new Person("Juan",26));
        persons.add(new Person("Pedro",32));
        persons.add(new Person("José",48));
        persons.add(new Person("Carmen",19));
        persons.add(new Person("Marta",29));
        persons.add(new Person("María",58));
        Person eldestPerson = persons.stream().max(byAge()).get();
        Person youngestPerson = persons.stream().min(byAge()).get();

        Integer value = persons.parallelStream().map(Person::age).filter(person->person<40).reduce(average()).get();
        double value2 = persons.stream().mapToInt(Person::age).average().getAsDouble();
        System.out.println(eldestPerson.name());
        System.out.println(youngestPerson.name());
        System.out.println(value);
        System.out.println(value2);

    }

    private static BinaryOperator<Integer> average(){
        return new BinaryOperator<Integer>() {
            @Override
            public Integer apply(Integer accumulated, Integer value) {
                return accumulated + value;
            }
        };
    }

    private static Comparator<? super Person> byAge() {
        return (o1, o2) -> o1.age() - o2.age();
    }
}
