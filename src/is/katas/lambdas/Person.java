package is.katas.lambdas;

/**
 * Created by usuario on 08/11/2016.
 */
public class Person {
    private final String name;
    private final int age;

    public String name() {
        return name;
    }

    public int age() {
        return age;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
