package is.katas.dates;

import java.time.*;

public class Main{
    public static void main(String[] args) {
        Instant instant = Instant.now();
        System.out.println(instant);
        ZonedDateTime x = instant.atZone(ZoneId.of("Poland"));
        System.out.println(x);
        LocalDateTime y = LocalDateTime.now();
        System.out.println(y);
        //2016-11-08T12:08:19.737Z
        //2016-11-08T13:08:19.737+01:00[Poland]
        //2016-11-08T13:08:19.830
        LocalDate date = LocalDate.now();
        System.out.println(date);
        //2016-11-08
        Instant date2 = date.atStartOfDay(ZoneId.of("Atlantic/Canary")).toInstant();
        System.out.println(date2);

    }
}